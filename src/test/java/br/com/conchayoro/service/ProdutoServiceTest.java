package br.com.conchayoro.service;

import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;

import br.com.conchayoro.entity.Produto;
import br.com.conchayoro.persistence.ProdutoDao;

@RunWith(EasyMockRunner.class)
public class ProdutoServiceTest {
		
	@TestSubject
    private ProdutoService produtoService = new ProdutoService();
	
	@Mock
	private ProdutoDao produtoDaoMock;
	
	@Mock
	private Logger logMock;
		
	private Produto produto; 
	
	@Before
	public void setup() {
		produto = new Produto();
		produto.setId(1L);
		produto.setNome("Nome1");
		produto.setPrecoUnitario(100.0);
		produto.setUnidade("cx");
		
	}
	
	@Test
	public void testIncluir() {
		produtoDaoMock.incluir(EasyMock.anyObject());
		replay(produtoDaoMock);
		assertEquals("Produto incluído com sucesso!", produtoService.incluir(produto));		
    	verify(produtoDaoMock);		
	}
	
	@Test
	public void testAlterar() {
		produtoDaoMock.alterar(EasyMock.anyObject());
		replay(produtoDaoMock);
		assertEquals("Produto alterado com sucesso!", produtoService.alterar(produto));		
		verify(produtoDaoMock);		
	}
	
	@Test
	public void testExcluir() {
		produtoDaoMock.excluir(EasyMock.anyObject());
		replay(produtoDaoMock);
		assertEquals("Produto excluído com sucesso!", produtoService.excluir(produto.getId()));		
		verify(produtoDaoMock);		
	}
	
	@Test
	public void testConsultar() {
		EasyMock.expect(produtoDaoMock.consultar(produto.getId())).andReturn(null);
		replay(produtoDaoMock);
		assertEquals(null, produtoService.consultar(produto.getId()));		
		verify(produtoDaoMock);		
	}
	
	@Test
	public void testListar() {
		EasyMock.expect(produtoDaoMock.listar()).andReturn(null);
		replay(produtoDaoMock);
		assertEquals(null, produtoService.listar());		
		verify(produtoDaoMock);		
	}
		
}
