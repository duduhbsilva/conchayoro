package br.com.conchayoro.persistence;

import java.util.List;

import br.com.conchayoro.entity.Produto;

public interface ProdutoDao {
	
	void incluir(Produto produto);
 
	void alterar(Produto produto);
	
	void excluir(Long codigo);
 
	List<Produto> listar(); 
	
	Produto consultar(Long codigo);

}

